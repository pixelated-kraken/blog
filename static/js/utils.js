/* This file has functions to create specific functionalities in the blog */

/** 
 * Function to add a random class of an array in specific class elements
*/
function AddRandomClassName(classes, classToGetElements) {
    var randomClass;
    var elements = document.getElementsByClassName(classToGetElements);
    for (var index = 0; index < elements.length; ++index) {
        randomClass = classes[Math.floor(Math.random() * classes.length)];
        elements[index].classList.add(randomClass);
    }
}

/**
 * Class to add specific base color in class elements "random_color"
 */
function AddClassRandomColor() {

    // Array containing color classes
    var colors = [
        'base07',
        'base08',
        'base09',
        'base0a',
        'base0b',
        'base0c',
        'base0d',
        'base0e',
        'base0f',
    ];

    // Insert in elements the random class color
    AddRandomClassName(colors, 'random_color');
}

/**
 * Class to add specific filter in class elements "random_filter"
 */
function AddClassRandomFilter() {
    // Array containing color filter classes
    var filters = [
        'base07-filter',
        'base08-filter',
        'base09-filter',
        'base0a-filter',
        'base0b-filter',
        'base0c-filter',
        'base0d-filter',
        'base0e-filter',
        'base0f-filter',
    ];

    // Insert in elements the random class color
    AddRandomClassName(filters, 'random_filter');
}

/**
 * Class to add specific hue filter in class elements "random_filter"
 */
function AddClassRandomHueFilter() {
    // Array containing color filter classes
    var filters = [
        'hue-0',
        'hue-45',
        'hue-90',
        'hue-135',
        'hue-180',
        'hue-225',
        'hue-270',
        'hue-315',
    ];

    // Insert in elements the random class color
    AddRandomClassName(filters, 'random_filter');
}