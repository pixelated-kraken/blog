+++
title = "New models"
description = "New models"
date = "2020-07-14"
author = "Mike"
tags = []
categories = ["Images"]
+++

Hi Platitudinous Wombat!

Today new models, coffeemaker, ministation controller and a few panels <br/>

<a href="/blog/img/posts/newmodels1/MiniStationController.PNG" target="_blank">
  <img src="/blog/img/posts/newmodels1/MiniStationController.PNG">
</a>
<a href="/blog/img/posts/newmodels1/cafetera.PNG" target="_blank">
  <img src="/blog/img/posts/newmodels1/cafetera.PNG">
</a>
<a href="/blog/img/posts/newmodels1/A1.PNG" target="_blank">
  <img src="/blog/img/posts/newmodels1/A1.PNG">
</a>
<a href="/blog/img/posts/newmodels1/SmartPanels.PNG" target="_blank">
  <img src="/blog/img/posts/newmodels1/SmartPanels.PNG">
</a>