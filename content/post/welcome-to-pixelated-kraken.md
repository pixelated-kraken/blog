+++
title = "Welcome to Pixelated Kraken blog"
description = "The first post of our blog. Welcome to Pixelated Kraken"
date = "2020-06-30"
author = "Pixelated Kraken"
tags = []
categories = []
+++

Hello! We are Jabi and Mike, and together we are Pixelated Kraken, welcome to our blog.

Pixelated Kraken is a project created with the purpose to reach a dream: develop videogames. This blog will contains posts about tools, assets and videogames developed by us.

So, stay tuned for future posts!

<div class="image-center"><img src="/blog/img/PixelatedKraken.png" alt="Pixelated Kraken Logo" class="image-center" ></div>