+++
title = "Youtube channel opening"
description = "The opening of the Pixelated Kraken youtube channel"
date = "2020-07-06"
author = "Pixelated Kraken"
tags = []
categories = ["Videos"]
+++

Greetings dowdy cephalopod!

We are glad to announce that we opened the [new Pixelated Kraken youtube channel](https://www.youtube.com/channel/UC2S8zlHJ17-b5Vufm-uwNVA). Here we will upload videos about our projects to show you the work in progress, assets, tools and the evolution of the team skills. In short, a place to dive into our worlds and give free rein to the creative imagination.

Finally, you can see our intro in the video below this text.

{{< youtube XypTMRFHEQo >}}