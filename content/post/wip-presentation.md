+++
title = "Work in progress"
description = "WIP"
date = "2020-07-07"
author = "Mike"
tags = []
categories = ["Images"]
+++

Hi tremolous humanoid!

Here we are glad to show you some work in progress captures about the current work of the team, at this momment we only can illustrate you with a few pictures but into the future we will explain what are we doing... <br/>

<a href="/blog/img/posts/wip/WIP.PNG" target="_blank">
  <img src="/blog/img/posts/wip/WIP.PNG">
</a>
<a href="/blog/img/posts/wip/WIP2.PNG" target="_blank">
  <img src="/blog/img/posts/wip/WIP2.PNG">
</a>
<a href="/blog/img/posts/wip/WIP3.PNG" target="_blank">
  <img src="/blog/img/posts/wip/WIP3.PNG">
</a>