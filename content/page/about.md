+++
title = "About"
author = "Pixelated Kraken"
description = "About us"
+++

Hello! We are Jabi and Mike, and together we are Pixelated Kraken.

Pixelated Kraken is a project created with the purpose to reach a dream: develop videogames. This blog contains posts about tools, assets and videogames developed by us.

## Jabi

<img class="about-avatar" src="/blog/img/About/Jabi-Avatar.png" />

Greetings Human! 

My name is Javier Osuna Herrera, but everybody calls me Jabi. I'm a Spanish videogames and virtual reality developer who is always studying and learning how to improve my skills in order to create better games and experiences. Since I played my first game in my parent's old computer, [Prince of Persia](https://en.wikipedia.org/wiki/Prince_of_Persia_(1989_video_game)), I knew that my work would be related to videogames.

My favorite videogames genre is RPG, but I play all types of games. I fell in love with Pokemon, Monster Hunter and Metal Gear. Also, I'm a good cook specialized in deserts, my best one is the Sand Cake.

<div class="about-personal-links">
<a href="https://es.linkedin.com/in/javier-osuna-herrera-15929289" aria-label="linkedin"><i class="base00 fab fa-linkedin fa-2x"></i></a>
<a href="https://www.dropbox.com/s/bldk9l8j2rl69ph/CV_English.pdf?dl=0" aria-label="resume"><i class="base00 fab fas fa-address-card fa-2x"></i></a>
<a href="https://gitlab.com/javosuher" aria-label="gitlab"><i class="base00 fab fa-gitlab fa-2x"></i></a>
<a href="https://github.com/javosuher" aria-label="github"><i class="base00 fab fa-github fa-2x"></i></a>
<a href="mailto:javier.osunaherrera@gmail.com" aria-label="email"><i class="base00 fa fa-envelope fa-2x"></i></a>
</div>

## Mike

<img class="about-avatar" src="/blog/img/About/Mike-Avatar.png" />

This is our world now... the world of the electron and the switch, beauty of the baud.

My name is Miguel Angel García, alias Mike. I'm a videogames developer like Jabi and i love making 3dmodels with blender, texturize with Substance Painter and pixel art.

<div class="about-personal-links">
<a href="mailto:supermiwer@gmail.com" aria-label="email"><i class="base00 fa fa-envelope fa-2x"></i></a>
</div>