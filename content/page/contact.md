+++
title = "Contact"
author = "Pixelated Kraken"
description = "How to contact us"
+++

If you have any question, please feel free to send us an email to: 
<a href="mailto:pixelated.kraken@gmail.com" aria-label="email">pixelated.kraken@gmail.com</a>